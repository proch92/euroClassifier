const express = require('express');
const app = express();
var httpProxy = require('http-proxy');
var apiProxy = httpProxy.createProxyServer();

app.use('/scripts', express.static(__dirname + '/node_modules/'));
app.use('/', express.static(__dirname));

app.all("/class/*", function(req, res){
	apiProxy.web(req, res, {target:'http://localhost:5000'});
});

app.listen(5001, function () {
	console.log('Example app listening on port 5001!');
});
