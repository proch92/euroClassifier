$(document).ready(function() {
	getVideo();

	resizeVideo();
});

var colorSetIndex = 0;

window.onload = function () {
	var colorSets = ["defaultColorSet", "deuColorSet", "proColorSet", "triColorSet"];

	CanvasJS.addColorSet(colorSets[0], [
	     "#b71c1c",
	     "#1a237e",
	     "#004d40",
	     "#ff6f00",
	     "#263238",
    ]);

    CanvasJS.addColorSet(colorSets[1], [
	     "#a50f15",
	     "#e1be91",
	     "#08519c",
	     "#c8ffff",
	     "#193c37",
    ]);

    CanvasJS.addColorSet(colorSets[2], [
	     "#a50f15",
	     "#fcae91",
	     "#29509c",
	     "#19b4d7",
	     "#192d23",
    ]);

    CanvasJS.addColorSet(colorSets[3], [
	     "#a50f15",
	     "#fab92d",
	     "#08519c",
	     "#afeb55",
	     "#695a8c",
    ]); 

	var chart = new CanvasJS.Chart("chart", {
		colorSet: "defaultColorSet",
		animationEnabled: true,
		exportEnabled: false,
		title: {
			text: "Employees Salary in a Company"
		},
		axisX: {
			title: "Departments"
		},
		axisY: {
			includeZero: false,
			title: "Salary in USD",
			interval: 10,
			suffix: "k",
			prefix: "$"
		}, 
		data: [{
			type: "rangeBar",
			showInLegend: true,
			yValueFormatString: "$#0.#K",
			indexLabel: "{y[#index]}",
			legendText: "Department wise Min and Max Salary",
			toolTipContent: "<b>{label}</b>: {y[0]} to {y[1]}",
			dataPoints: [
				{ x: 10, y:[80, 115], label: "Data Scientist"},
				{ x: 20, y:[95, 141], label: "Product Manager"},
				{ x: 30, y:[98, 115], label: "Web Developer"},
				{ x: 40, y:[90, 160], label: "Software Engineer"},
				{ x: 50, y:[100,152], label: "Quality Assurance"}
			]
		}]
	});
	chart.render();

	$("#chart").click(function() {
		colorSetIndex = (colorSetIndex + 1) % colorSets.length;
		chart.set("colorSet", colorSets[colorSetIndex]);
	});
}

//resize event handler
$(window).resize(function () {
	resizeVideo();
});

// gets the webcam input stream
window.getVideo = function () {
	var video = document.querySelector('video');
	navigator.mediaDevices.getUserMedia({video: true}).then(function(stream) {
		console.log(stream);
		video.srcObject = stream;
		video.onloadedmetadata = function(e) {video.play();};
	}).catch(function(err) {
		console.log(err);
	});
}

window.resizeVideo = function () {
	var w = $(window).width();
	var h = $(window).height();
	$("#cam").width(w);
	$("#cam").height(h);
	$("#commands").width(w);
	$("#commands").height(h);
	$("#spacer").width(w);
	$("#spacer").height(h);
}

// takes a screenshot from the video source, resize it to 100x100, encodes it and sends it to the python server
window.snapshot = function () {
	var video = document.querySelector('video');
	var canvas = document.querySelector('canvas');

	canvas.getContext('2d').drawImage(video, 0, 0);
	
	var data = canvas.toDataURL('image/png');

	Jimp.read(data).then(function(img) {
		img.resize(224,224)
		
		buffer = img.bitmap.data
		var b64data = buffer.toString('base64');
		$.post({
			url: 'class/predict',
			data: b64data,
			cache: false,
			contentType: 'text/json',
			processData: false,
			success: function(response) {
				console.log(response);
				$('#prediction').html(response.prediction);
				responsiveVoice.speak(response.prediction.toString());
			}
		});
	}).catch(function(err) {
		console.log("caught: " + err);
	});
}

prediction = false;
window.command = function() {
	if (prediction == false) {
		prediction = true;
		snapshot();
		$("#commands div").fadeTo(200, 0.9);
		$("#prediction").fadeTo(200, 1);
	} else {
		prediction = false;
		$("#commands div").fadeTo(200, 0.0);
		$("#prediction").fadeTo(200, 0.0);
	}
}