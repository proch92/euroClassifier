import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.models as models
import torchvision.transforms as transforms
import torchvision.datasets as dset
import time
import os
import numpy as np

class CnnModel(nn.Module):
	# in 	224*224*3
	# conv 	(224-5+1)*(224-5+1)*6 = 220*220*6
	# pool 	110*110*6
	# conv 	(110-5+1)*(110-5+1)*12 = 106*106*12
	# pool 	53*53*12
	# view	33708

	def __init__(self):
		super(CnnModel, self).__init__()
		self.conv1 = nn.Conv2d(3, 6, kernel_size=5)
		self.conv2 = nn.Conv2d(6, 12, kernel_size=5)
		self.conv2_drop = nn.Dropout2d()
		self.fc1 = nn.Linear(33708, 70)
		self.fc2 = nn.Linear(70, 7)

	def forward(self, x):
		x = F.relu(F.max_pool2d(self.conv1(x), 2))
		x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
		x = x.view(-1, 33708)
		x = F.relu(self.fc1(x))
		x = F.dropout(x, training=self.training)
		x = self.fc2(x)
		return F.log_softmax(x, dim=1)

class Vision():
	def __init__(self, model_name):
		self.model_name = model_name
		self.width = 224
		self.height = 224

		self.model = CnnModel()

		if os.path.isfile('model/{}'.format(self.model_name)):
			print('loading model from fs')
			self.load_model()

		self.transform = transforms.Compose([
						transforms.Resize((224,224)),
						transforms.ToTensor(),
						transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
					])

		validation_ratio = 0.2
		dataset = dset.ImageFolder(root="res/orig/", transform=self.transform)
		validation_size = int(np.floor(len(dataset) * validation_ratio))
		train_size = len(dataset) - validation_size
		trainset, validationset = torch.utils.data.random_split(dataset, [train_size, validation_size])
		self.trainloader = torch.utils.data.DataLoader(trainset, shuffle=True)
		self.validationloader = torch.utils.data.DataLoader(validationset, shuffle=True)


	def train(self):

		self.optimizer = optim.SGD(self.model.parameters(), lr=0.001, momentum=0.9)

		self.model.train()

		for data, target in self.trainloader:
			self.optimizer.zero_grad()
			output = self.model(data)
			loss = F.nll_loss(output, target)
			loss.backward()
			self.optimizer.step()


	def validate(self):
		self.model.eval()

		test_loss = 0
		correct = 0
		with torch.no_grad():
			for data, target in self.validationloader:
				output = self.model(data)
				test_loss += F.nll_loss(output, target)
				pred = output.max(1, keepdim=True)[1]
				correct += pred.eq(target.view_as(pred)).sum().item()

		test_loss /= len(self.validationloader.dataset)
		print('Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)'.format(test_loss, correct, len(self.validationloader.dataset), 100. * correct / len(self.validationloader.dataset)))


	def eval(self):
		self.model.eval()

		evalset = dset.ImageFolder(root="res/eval/", transform=self.transform)
		evalloader = torch.utils.data.DataLoader(evalset)

		image, _ = iter(evalloader).next()

		output = self.model.forward(image)
		ps = torch.exp(output)
		print('out:')
		print(ps)
		print(int(ps.max(dim=1)[1]))

		return int(ps.max(dim=1)[1])


	def save_model(self):
		torch.save(self.model.state_dict(), 'model/{}'.format(self.model_name))


	def load_model(self):
		self.model.load_state_dict(torch.load('model/{}'.format(self.model_name)))


if __name__ == '__main__':
	import sys

	vision = Vision(sys.argv[1])
	epochs = int(sys.argv[2])

	start = time.time()

	for ep in range(epochs):
		print("epoch {} / {}".format(ep+1, epochs))
		vision.train()
		vision.validate()

	end = time.time()
	print("execution time: ", end - start)

	vision.save_model()
