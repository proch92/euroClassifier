#!/usr/bin/python3

from flask import Flask
from flask import jsonify
from flask import request
import base64
import hashlib
import numpy as np
import io
from PIL import Image
import struct
from vision import Vision
import image_utils as util

app = Flask("classifierApp")

@app.route('/')
def index():
	return "Hello, World!"

@app.route('/class/predict', methods=['POST'])
def predict():
	image = request.data
	image = base64.b64decode(image)
	im = Image.frombytes('RGBA', (224,224), image, 'raw')
	im.save('res/eval/flask/test.png')

	pred = 0
	with app.app_context():
		pred = vision.eval()

	print("prediction: {}".format(pred))

	return jsonify(prediction=pred)


if __name__ == "__main__":
	with app.app_context():
		vision = Vision('testtorch2')
	app.run(debug=True, host='127.0.0.1', port=5000)