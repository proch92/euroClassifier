from scipy import misc
import numpy as np
from pathlib import Path

conversion_dict = {
	5: 0,
	10: 1,
	20: 2,
	50: 3,
	100: 4,
	200: 5,
	500: 6
}

conversion_dict_rev = {
	0: 5,
	1: 10,
	2: 20,
	3: 50,
	4: 100,
	5: 200,
	6: 500
}


def save(filename, image):
	misc.imsave(filename, image)


def load(path):
	return misc.imread(path, mode='RGB')


def preprocess_image(image):
	f = np.vectorize(lambda c: c/255)
	image = f(image)

	return image


def preprocess_y(y):
	if isinstance(y, str):
		y = int(y)

	array = np.zeros(len(conversion_dict))

	array[conversion_dict[y]] = 1

	return array


def preprocess(batch, y=[]):
	batch = [ preprocess_image(image) for image in batch ]
	y = [ preprocess_y(n) for n in y ]

	return (batch, y)


def classes_to_value(classes):
	idx = np.argmax(classes)
	return conversion_dict_rev[idx]


def rot90(image):
	return misc.imrotate(image, 90)


def convert(filename, shape):
	image = load(filename)
	image = misc.imresize(image, shape, 'bilinear')

	return image
