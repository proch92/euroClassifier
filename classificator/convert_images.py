from scipy import misc
from pathlib import Path
import image_utils as img
import hashlib
import os
from base64 import b64encode

for dir in os.listdir(os.path.join('res', 'orig')):
	for file in os.listdir(os.path.join('res', 'orig', dir)):
		if os.path.isfile(os.path.join('res', 'orig', dir, file)):
			md5 = hashlib.md5()
			r = b64encode(os.urandom(64)).decode('utf-8')
			md5.update((dir + file + r).encode('utf-8'))
			im = img.convert(os.path.join('res', 'orig', dir, file), (224,224))
			img.save(os.path.join('res', 'dataset224', dir, dir + '_1' + md5.hexdigest() + '.jpg'), im)
			im = img.rot90(im)
			img.save(os.path.join('res', 'dataset224', dir, dir + '_2' + md5.hexdigest() + '.jpg'), im)
			im = img.rot90(im)
			img.save(os.path.join('res', 'dataset224', dir, dir + '_3' + md5.hexdigest() + '.jpg'), im)
			im = img.rot90(im)
			img.save(os.path.join('res', 'dataset224', dir, dir + '_4' + md5.hexdigest() + '.jpg'), im)